/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.security;

/**
 * Represens the digest algorithm name.
 *
 * @author Thomás Sousa Silva
 */
public enum DigestAlgorithm {

	/**
	 * MD2
	 */
	MD2("MD2"),
	/**
	 * MD5
	 */
	MD5("MD5"),
	/**
	 * SHA-1
	 */
	SHA_1("SHA-1"),
	/**
	 * SHA-224
	 */
	SHA_224("SHA-224"),
	/**
	 * SHA-256
	 */
	SHA_256("SHA-256"),
	/**
	 * SHA-384
	 */
	SHA_384("SHA-384"),
	/**
	 * SHA-512
	 */
	SHA_512("SHA-512");

	private final String ALGORITHM_NAME;

	private DigestAlgorithm(String algorithmName) {
		ALGORITHM_NAME = algorithmName;
	}

	/**
	 * @return The algorithm name.
	 */
	@Override
	public String toString() {
		return ALGORITHM_NAME;
	}

}
