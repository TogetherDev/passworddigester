/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.security;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.function.BiConsumer;

/**
 * <p>
 * Represents a password digester. This interface abstracts the digestion of password with salt.
 * </p>
 * <p>
 * Note: <strong>Use a static final instance</strong> for it to keep the randomness and consistency.
 * </p>
 * <div>
 * <h2>Use example:</h2>
 *
 * <pre>
 * <code>
 * public class User {
 *
 *     private static final {@linkplain PasswordDigester} PASSWORD_DIGESTER = {@linkplain PasswordDigester}.{@linkplain PasswordDigester#builder() builder}()
 *             .{@linkplain Builder#setSaltLength(int, int) setSaltLength}(20, 26)
 *             .{@linkplain Builder#setDigestAlgorithm(com.togetherdev.util.security.DigestAlgorithm) setDigestAlgorithm}({@linkplain DigestAlgorithm#SHA_512 SHA_512})
 *             .{@linkplain Builder#setSecureRandomizer(com.togetherdev.util.security.SecureRandomAlgorithm) setSecureRandomizer}({@linkplain SecureRandomAlgorithm#SHA1PRNG SHA1PRNG})
 *             .{@linkplain Builder#setSeedDuration(long, int) setSeedDuration}(1_000_000, 10) //Optional
 *             .{@linkplain Builder#build() build}();
 *
 *     private String password;
 *     private String salt;
 *
 *     public void setPassword(String password) {
 *         PASSWORD_DIGESTER.{@linkplain #setPassword(java.lang.String, java.util.function.BiConsumer) setPassword}(password, (p, s) -> {
 *             this.password = p;
 *             this.salt = s;
 *         });
 *     }
 *
 *     public boolean passwordEquals(String password) {
 *         return PASSWORD_DIGESTER.{@linkplain #passwordEquals(java.lang.String, java.lang.String, java.lang.String) passwordEquals}(this.password, password, salt);
 *     }
 *
 * }
 * </code>
 * </pre>
 *
 * </div>
 *
 * @see java.security.MessageDigest MessageDigest
 * @see java.security.SecureRandom SecureRandom
 * @author Thomás Sousa Silva
 */
public interface PasswordDigester {

	/**
	 * The default password charset, that is used to decode the passwords.
	 *
	 * @see java.nio.charset.StandardCharsets#UTF_8 UTF_8
	 */
	public static final Charset DEFAULT_PASSWORD_CHARSET = UTF_8;

	/**
	 * The default password encoder, that is used to encode the digested password.
	 *
	 * @see java.util.Base64#getEncoder() getEncoder
	 */
	public static final Encoder DEFAULT_PASSWORD_ENCODER = Base64.getEncoder();

	/**
	 * The default salt encoder, that is used to encode the {@linkplain SecureRandomizer#generateRandomArray() generated} salt.
	 *
	 * @see java.util.Base64#getEncoder() getEncoder
	 */
	public static final Encoder DEFAULT_SALT_ENCODER = Base64.getEncoder();

	/**
	 * The default salt decoder, that is used to decode the salts.
	 *
	 * @see java.util.Base64#getDecoder() getDecoder
	 */
	public static final Decoder DEFAULT_SALT_DECODER = Base64.getDecoder();

	/**
	 * Constructs a new instance of Builder with some values defined by default.
	 *
	 * @return The default builder.
	 * @see #DEFAULT_SALT_ENCODER salt encoder
	 * @see #DEFAULT_SALT_DECODER salt decoder
	 * @see #DEFAULT_PASSWORD_CHARSET password charset
	 * @see #DEFAULT_PASSWORD_ENCODER password encoder
	 * @see DefaultPasswordDigester DefaultPasswordDigester
	 */
	public static Builder builder() {
		return new DefaultPasswordDigester.Builder();
	}

	/**
	 * Generate a digested password with salt.
	 *
	 * @param password The password that will be used to generate the digested password.
	 * @param salt The salt that will be used to generate the digested password.
	 * @return The digested password.
	 * @throws java.lang.NullPointerException If some arguments is null.
	 * @throws java.lang.IllegalArgumentException If the password is empty or if the salt length is less than two.
	 * @throws java.lang.UnsupportedOperationException If the {@linkplain #getDigestAlgorithm() digest algorithm} is not available in the environment.
	 * @see #digest(java.lang.String, java.lang.String) digest
	 */
	public byte[] digest(byte[] password, byte[] salt);

	/**
	 * Generate a digested password with salt.
	 *
	 * @param password The password that will be used to generate the digested password.
	 * @param salt The salt that will be used to generate the digested password.
	 * @return The digested password.
	 * @throws java.lang.NullPointerException If some arguments is null.
	 * @see #digest(byte[], byte[]) digest
	 */
	public byte[] digest(String password, String salt);

	/**
	 * Returns the name of the algorithm that this instance use to {@linkplain java.security.MessageDigest#digest(byte[]) digest} the passwords.
	 *
	 * @return The name.
	 */
	public String getDigestAlgorithm();

	/**
	 * Returns the password charset. It's used to decode the passwords.
	 *
	 * @return The passwords charset.
	 * @see String#getBytes(java.nio.charset.Charset) getBytes
	 */
	public Charset getPasswordCharset();

	/**
	 * Returns the password encoder, that is used to encode the digested password.
	 *
	 * @return The password encoder.
	 * @see Encoder#encodeToString(byte[]) encodeToString
	 */
	public Encoder getPasswordEncoder();

	/**
	 * Returns the salt decoder, that is used to decode the salts.
	 *
	 * @return The salt decoder.
	 * @see Decoder#decode(byte[]) decode
	 */
	public Decoder getSaltDecoder();

	/**
	 * Returns the default salt encoder, that is used to encode the {@linkplain SecureRandomizer#generateRandomArray() generated} salt.
	 *
	 * @return The salt encoder.
	 * @see Encoder#encodeToString(byte[]) encodeToString
	 */
	public Encoder getSaltEncoder();

	/**
	 * @return The secure randomizer.
	 */
	public SecureRandomizer getSecureRandomizer();

	/**
	 * <p>
	 * Returns true if the {@code password} more the {@code salt} is equal to the {@code digestedPassword}.
	 * </p>
	 * <div> Note:
	 * <ul>
	 * <li>If the {@code digestedPassword} and the {@code password} is null, then returns true.</li>
	 * <li>If the {@code digestedPassword} is non null and the {@code password} or {@code salt} is null, then returns false.</li>
	 * </ul>
	 * </div>
	 *
	 * @param digestedPassword The digested password that will be used to comparison.
	 * @param password The password that will be used to generate the digested password.
	 * @param salt The salt that will be used to generate the digested password.
	 * @return True if the {@code password} more the {@code salt} is equal to the {@code digestedPassword}.
	 * @see PasswordDigester use example
	 */
	public boolean passwordEquals(String digestedPassword, String password, String salt);

	/**
	 * <p>
	 * Generates a digested password for the {@code newPassword} using a {@linkplain SecureRandomizer#generateRandomArray() new salt} and delegate to
	 * the {@code setter} the function of set the new password and the new salt.
	 * </p>
	 * <div> Note:
	 * <ul>
	 * <li>The {@code setter} will {@linkplain java.util.function.BiConsumer#accept(java.lang.Object, java.lang.Object) receive} the new digested
	 * password and the salt respectively. ({@code setter.accept(digestedPassword, salt)})</li>
	 * <li>If the {@code newPassword} is null then the setter will receive null. ({@code setter.accept(null, null)})</li>
	 * </ul>
	 * </div>
	 *
	 * @param newPassword The password that will be used to generate digested password.
	 * @param setter The setter that will set the new password.
	 * @throws NullPointerException If the setter is null.
	 * @see PasswordDigester use example
	 */
	public void setPassword(String newPassword, BiConsumer<String, String> setter);

	/**
	 * This class is useful to builder a instances of PasswordDigester.
	 */
	public static abstract class Builder {

		protected Encoder saltEncoder;
		protected Decoder saltDecoder;
		protected String digestAlgorithm;
		protected Charset passwordCharset;
		protected Encoder passwordEncoder;
		protected SecureRandomizer.Builder secureRandomizerBuilder;

		/**
		 * Constructs a new instance of Builder with some values defined by default.
		 *
		 * @see #DEFAULT_SALT_ENCODER salt encoder
		 * @see #DEFAULT_SALT_DECODER salt decoder
		 * @see #DEFAULT_PASSWORD_CHARSET password charset
		 * @see #DEFAULT_PASSWORD_ENCODER password encoder
		 */
		public Builder() {
			saltEncoder = DEFAULT_SALT_ENCODER;
			saltDecoder = DEFAULT_SALT_DECODER;
			passwordCharset = DEFAULT_PASSWORD_CHARSET;
			passwordEncoder = DEFAULT_PASSWORD_ENCODER;
			secureRandomizerBuilder = SecureRandomizer.builder();
		}

		/**
		 * Constructs a new instance of PasswordDigester with the defined values in this instance.
		 *
		 * @return The new instance.
		 */
		public abstract PasswordDigester build();

		/**
		 * Sets the digest algorithm.
		 *
		 * @param digestAlgorithm The algorithm.
		 * @return This.
		 * @throws NullPointerException if the digestAlgorithm is null.
		 */
		public Builder setDigestAlgorithm(DigestAlgorithm digestAlgorithm) {
			return setDigestAlgorithm(digestAlgorithm.toString());
		}

		/**
		 * Sets the digest algorithm.
		 *
		 * @param digestAlgorithm The algorithm name.
		 * @return This.
		 * @throws NullPointerException if the digestAlgorithm is null.
		 */
		public Builder setDigestAlgorithm(String digestAlgorithm) {
			this.digestAlgorithm = requireNonNull(digestAlgorithm);
			return this;
		}

		/**
		 * Sets the password charset.
		 *
		 * @param passwordCharset The new charset.
		 * @return This.
		 * @see #DEFAULT_PASSWORD_CHARSET default password charset
		 */
		public Builder setPasswordCharset(Charset passwordCharset) {
			this.passwordCharset = requireNonNull(passwordCharset);
			return this;
		}

		/**
		 * Sets the password encoder.
		 *
		 * @param passwordEncoder The new encoder.
		 * @throws NullPointerException if the password encoder is null.
		 * @return This.
		 * @see #DEFAULT_PASSWORD_ENCODER default password encoder
		 */
		public Builder setPasswordEncoder(Encoder passwordEncoder) {
			this.passwordEncoder = requireNonNull(passwordEncoder);
			return this;
		}

		/**
		 * Sets the salt encoder.
		 *
		 * @param saltDecoder The new decoder.
		 * @return This.
		 * @throws NullPointerException if the decoder is null.
		 * @see #DEFAULT_SALT_DECODER default salt decoder
		 */
		public Builder setSaltDecoder(Decoder saltDecoder) {
			this.saltDecoder = requireNonNull(saltDecoder);
			return this;
		}

		/**
		 * Sets the salt encoder.
		 *
		 * @param saltEncoder The new encoder.
		 * @return This.
		 * @throws NullPointerException if the encoder is null.
		 * @see #DEFAULT_SALT_ENCODER default salt encoder
		 */
		public Builder setSaltEncoder(Encoder saltEncoder) {
			this.saltEncoder = requireNonNull(saltEncoder);
			secureRandomizerBuilder.setEncoder(saltEncoder);
			return this;
		}

		/**
		 * Sets the minimum and maximum salt length that the PasswordDigester will {@linkplain SecureRandomizer#generateRandomArray() generate}.
		 *
		 * @param minLength The minimum length (inclusive).
		 * @param maxLength The maxLength length (exclusive).
		 * @return This.
		 * @throws java.lang.IllegalArgumentException If the {@code minLength} is less than two or if the {@code maxLength} is less than
		 *             {@code minLength}.
		 * @see SecureRandomizer#getMinLength() getMinLength
		 * @see SecureRandomizer#getMaxLength() getMaxLength
		 */
		public Builder setSaltLength(int minLength, int maxLength) {
			secureRandomizerBuilder.setCodeLength(minLength, maxLength);
			return this;
		}

		/**
		 * Sets the secure randomizer.
		 *
		 * @param secureRandomizer The new randomizer.
		 * @return This.
		 * @throws NullPointerException if the secureRandomizer is null.
		 */
		public Builder setSecureRandomizer(SecureRandom secureRandomizer) {
			secureRandomizerBuilder.setSecureRandomizer(secureRandomizer);
			return this;
		}

		/**
		 * Sets the secure randomizer.
		 *
		 * @param secureRandomAlgorithm The new randomizer.
		 * @return This.
		 * @throws NullPointerException if the secureRandomAlgorithm is null.
		 * @see SecureRandom#getInstance(java.lang.String) getInstance
		 */
		public Builder setSecureRandomizer(SecureRandomAlgorithm secureRandomAlgorithm) {
			secureRandomizerBuilder.setSecureRandomizer(secureRandomAlgorithm);
			return this;
		}

		/**
		 * Sets the secure randomizer.
		 *
		 * @param secureRandomAlgorithm The new algorithm.
		 * @return This.
		 * @throws NullPointerException if the secureRandomAlgorithm is null.
		 * @see SecureRandom#getInstance(java.lang.String) getInstance
		 * @throws java.lang.RuntimeException If the secure random algorithm is not available in the environment.
		 */
		public Builder setSecureRandomizer(String secureRandomAlgorithm) {
			secureRandomizerBuilder.setSecureRandomizer(secureRandomAlgorithm);
			return this;
		}

		/**
		 * <p>
		 * Sets the duration that this instance can generate with the same seed.
		 * </p>
		 * <div> Note:
		 * <ul>
		 * <li>If the duration is less than one, then seed never will change by the PasswordDigester.</li>
		 * <li>The seed will be generate using the {@linkplain java.security.SecureRandom#generateSeed(int) generateSeed method}.</li>
		 * <li>By default the seed never will change by the PasswordDigester.</li>
		 * </ul>
		 * </div>
		 *
		 * @param duration The amount salts that the PasswordDigester can generate with the same seed.
		 * @param seedLength The length that is used to generate the seed.
		 * @return This.
		 * @see SecureRandomizer#getSeedDuration() getSeedDuration
		 * @see java.security.SecureRandom#setSeed(byte[]) setSeed
		 * @throws java.lang.IllegalArgumentException If the seed length is less than one.
		 */
		public Builder setSeedDuration(long duration, int seedLength) {
			secureRandomizerBuilder.setSeedDuration(duration, seedLength);
			return this;
		}

	}

}
