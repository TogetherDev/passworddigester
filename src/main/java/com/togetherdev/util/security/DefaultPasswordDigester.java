/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.security;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.function.BiConsumer;

/**
 * This class is useful to digest a password with salt.
 *
 * @author Thomás Sousa Silva
 */
public class DefaultPasswordDigester implements PasswordDigester {

	private final Encoder SALT_ENCODER;
	private final Decoder SALT_DECODER;
	private final String DIGEST_ALGORITHM;
	private final Charset PASSWORD_CHARSET;
	private final Encoder PASSWORD_ENCODER;
	private final SecureRandomizer SECURE_RANDOMIZER;

	/**
	 * Constructs a new instance of DefaultPasswordDigester with the builder config.
	 *
	 * @param builder The builder with the desired config.
	 * @throws NullPointerException If the builder is null.
	 */
	protected DefaultPasswordDigester(PasswordDigester.Builder builder) {
		SALT_ENCODER = builder.saltEncoder;
		SALT_DECODER = builder.saltDecoder;
		DIGEST_ALGORITHM = builder.digestAlgorithm;
		PASSWORD_CHARSET = builder.passwordCharset;
		PASSWORD_ENCODER = builder.passwordEncoder;
		SECURE_RANDOMIZER = builder.secureRandomizerBuilder.build();
	}

	@Override
	public byte[] digest(byte[] password, byte[] salt) {
		if ((password.length == 0) || (salt.length < 2)) {
			throw new IllegalArgumentException("The password is empty or if the salt length is less than two.");
		}
		try {
			MessageDigest messageDigest = MessageDigest.getInstance(DIGEST_ALGORITHM);
			messageDigest.update(salt);
			return messageDigest.digest(password);
		} catch (NoSuchAlgorithmException ex) {
			throw new UnsupportedOperationException(ex);
		}
	}

	@Override
	public byte[] digest(String password, String salt) {
		return digest(password.getBytes(PASSWORD_CHARSET), SALT_DECODER.decode(salt));
	}

	@Override
	public String getDigestAlgorithm() {
		return DIGEST_ALGORITHM;
	}

	@Override
	public Charset getPasswordCharset() {
		return PASSWORD_CHARSET;
	}

	@Override
	public Encoder getPasswordEncoder() {
		return PASSWORD_ENCODER;
	}

	@Override
	public Decoder getSaltDecoder() {
		return SALT_DECODER;
	}

	@Override
	public Encoder getSaltEncoder() {
		return SALT_ENCODER;
	}

	@Override
	public SecureRandomizer getSecureRandomizer() {
		return SECURE_RANDOMIZER;
	}

	@Override
	public boolean passwordEquals(String digestedPassword, String password, String salt) {
		if (digestedPassword == null) {
			return (password == null);
		} else if ((password == null) || (salt == null)) {
			return false;
		}
		String newDigestedPassword = PASSWORD_ENCODER.encodeToString(digest(password, salt));
		return digestedPassword.equals(newDigestedPassword);
	}

	@Override
	public void setPassword(String newPassword, BiConsumer<String, String> setter) {
		String digestedPassword = null, salt = null;
		if (newPassword != null) {
			byte[] saltBytes = SECURE_RANDOMIZER.generateRandomArray();
			digestedPassword = PASSWORD_ENCODER.encodeToString(digest(newPassword.getBytes(PASSWORD_CHARSET), saltBytes));
			salt = SALT_ENCODER.encodeToString(saltBytes);
		}
		setter.accept(digestedPassword, salt);
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("DefaultPasswordDigester [SALT_ENCODER=").append(SALT_ENCODER)
				.append(", SALT_DECODER=").append(SALT_DECODER)
				.append(", DIGEST_ALGORITHM=").append(DIGEST_ALGORITHM)
				.append(", PASSWORD_CHARSET=").append(PASSWORD_CHARSET)
				.append(", PASSWORD_ENCODER=").append(PASSWORD_ENCODER)
				.append(", SECURE_RANDOMIZER=").append(SECURE_RANDOMIZER)
				.append("]").toString();
	}

	/**
	 * The default builder of instances of DefaultPasswordDigester.
	 */
	public static class Builder extends PasswordDigester.Builder {

		@Override
		public PasswordDigester build() {
			return new DefaultPasswordDigester(this);
		}

	}

}
