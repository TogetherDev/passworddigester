/*
 * Copyright 2017 Thomás Sousa Silva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.togetherdev.util.security;

import static com.togetherdev.util.security.DigestAlgorithm.SHA_512;
import static com.togetherdev.util.security.PasswordDigester.DEFAULT_SALT_ENCODER;
import static com.togetherdev.util.security.SecureRandomAlgorithm.SHA1PRNG;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64.Encoder;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Thomás Sousa Silva
 */
public class DefaultPasswordDigesterTest {

	private static boolean printPasswords;
	private final int SEED_LENGTH = 10;
	private final int MAX_SALT_LENGTH = 25;
	private final int MIN_SALT_LENGTH = 20;
	private final long SEED_DURATION = 0;
	private final DigestAlgorithm DIGEST_ALGORITHM = SHA_512;
	private final SecureRandomAlgorithm SECURE_RANDOM_ALGORITHM = SHA1PRNG;

	@Test
	public void testGenerateNewRandomSalt() {
		System.out.println("generateNewRandomSalt");
		PasswordDigester instance = getDefaultInstance();

		byte[] lastResult = null;
		for (int i = 0; i < 10; i++) {
			byte[] result = instance.getSecureRandomizer().generateRandomArray();
			assertTrue(result.length >= MIN_SALT_LENGTH);
			assertTrue(result.length < MAX_SALT_LENGTH);
			if (lastResult != null) {
				Assert.assertNotSame(lastResult, result);
				assertFalse(Arrays.equals(lastResult, result));
			}
			lastResult = result;

		}
	}

	@Test
	public void testGetDigestAlgorithm() {
		System.out.println("getDigestAlgorithm");
		PasswordDigester instance = getDefaultInstance();
		String expResult = DIGEST_ALGORITHM.toString();
		String result = instance.getDigestAlgorithm();
		assertEquals(expResult, result);
	}

	@Test
	public void testGetMaxSaltLength() {
		System.out.println("getMaxSaltLength");
		PasswordDigester instance = getDefaultInstance();
		int expResult = MAX_SALT_LENGTH;
		int result = instance.getSecureRandomizer().getMaxLength();
		assertEquals(expResult, result);
	}

	@Test
	public void testGetMinSaltLength() {
		System.out.println("getMinSaltLength");
		PasswordDigester instance = getDefaultInstance();
		int expResult = MIN_SALT_LENGTH;
		int result = instance.getSecureRandomizer().getMinLength();
		assertEquals(expResult, result);
	}

	@Test
	public void testGetSecureAlgorithm() {
		System.out.println("getSecureAlgorithm");
		PasswordDigester instance = getDefaultInstance();
		String expResult = SECURE_RANDOM_ALGORITHM.toString();
		String result = instance.getSecureRandomizer().getSecureAlgorithm();
		assertEquals(expResult, result);
	}

	@Test
	public void testGetSeedDuration() {
		System.out.println("getSeedDuration");
		PasswordDigester instance = getDefaultInstance();
		long expResult = SEED_DURATION;
		long result = instance.getSecureRandomizer().getSeedDuration();
		assertEquals(expResult, result);
	}

	@Test
	public void testGetSeedLength() {
		System.out.println("getSeedLength");
		PasswordDigester instance = getDefaultInstance();
		int expResult = SEED_LENGTH;
		int result = instance.getSecureRandomizer().getSeedLength();
		assertEquals(expResult, result);
	}

	@Test
	public void testPasswordDigester() {
		DigestAlgorithm[] algorithms = DigestAlgorithm.values();
		for (DigestAlgorithm algorithm : algorithms) {
			System.out.println("test" + algorithm + "PasswordDigester");
			testPasswordDigester(getDefaultBuilder().setDigestAlgorithm(algorithm).build());
		}
	}

	private PasswordDigester.Builder getDefaultBuilder() {
		return PasswordDigester.builder()
				.setSaltLength(MIN_SALT_LENGTH, MAX_SALT_LENGTH)
				.setDigestAlgorithm(DIGEST_ALGORITHM)
				.setSecureRandomizer(SECURE_RANDOM_ALGORITHM)
				.setSeedDuration(SEED_DURATION, SEED_LENGTH);
	}

	private PasswordDigester getDefaultInstance() {
		return getDefaultBuilder().build();
	}

	private void testPasswordDigester(PasswordDigester digester) {
		User user = new User();
		for (int i = 0; i < 100; i++) {
			user.generateNewCredentials();
			user.testSetPassword(digester);
			user.testDigest(digester);
			user.testPasswordEquals(digester);
			if (printPasswords) {
				System.out.println(String.format("password = \"%s\"\nsalt = \"%s\"\nDigested password = \"%s\"", user.password, user.salt,
						user.digestedPassword));
			}
		}
	}

	private static final class User {

		private String password;
		private String digestedPassword;
		private String salt;

		private void generateNewCredentials() {
			password = generateRandomPassword();
			salt = generateRandomSalt();
		}

		private String generateRandomPassword() {
			ThreadLocalRandom random = ThreadLocalRandom.current();
			byte[] bytes = new byte[random.nextInt(1, 50)];
			random.nextBytes(bytes);
			return new String(bytes, StandardCharsets.UTF_8);
		}

		private String generateRandomSalt() {
			ThreadLocalRandom random = ThreadLocalRandom.current();
			byte[] bytes = new byte[random.nextInt(2, 50)];
			random.nextBytes(bytes);
			return DEFAULT_SALT_ENCODER.encodeToString(bytes);
		}

		private void testDigest(PasswordDigester digester) {
			byte[] bytes1 = digester.digest(password, salt);
			byte[] bytes2 = digester.digest(password, salt);
			assertArrayEquals(bytes1, bytes2);
		}

		private void testPasswordEquals(PasswordDigester digester) {
			boolean passwordEquals1 = digester.passwordEquals(digestedPassword, password, salt);
			boolean passwordEquals2 = digester.passwordEquals(digestedPassword, password, salt);
			assertTrue(passwordEquals1);
			assertTrue(passwordEquals2);
		}

		private void testSetPassword(PasswordDigester digester) {
			digester.setPassword(password, (p, s) -> {
				Encoder passwordEncoder = digester.getPasswordEncoder();
				String digestedPassword2 = passwordEncoder.encodeToString(digester.digest(password, s));
				assertEquals(p, digestedPassword2);
				digestedPassword = p;
				salt = s;
			});
		}

	}

}
