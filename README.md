# Password Digester #

#Versions

* **1.1**
* 1.0

## What is it?

This is a utility library to digest a password with salt. It generates a random salt of random length, for each given password and then, digests this two value using a given digestion algorithm, SHA-256 per example, that is provided by a *native security provider*.

## Using Password Digester

You can use our maven repository, that is easier, or you can download it and build the Jar file and later add to your project.

## Maven Repository

If in your pom.xml file have not the [TogetherDev Maven Repository](https://bitbucket.org/TogetherDev/mavenrepository) then, add this code.

```
#!xml

<repository>
    <id>togetherdev</id>
    <url>https://bitbucket.org/TogetherDev/mavenrepository/raw/master</url>
</repository>

```

And then, add this dependency:

```
#!xml
<dependency>
    <groupId>com.togetherdev</groupId>
    <artifactId>password-digester</artifactId>
    <version>1.1</version>
</dependency>
```

## Use example:

```
#!java

 public class User {

     private static final PasswordDigester PASSWORD_DIGESTER = PasswordDigester.builder()
             .setSaltLength(20, 26)
             .setDigestAlgorithm(SHA_512)
             .setSecureRandomizer(SHA1PRNG)
             .setSeedDuration(1_000_000, 10) //Optional
             .build();

     private String password;
     private String salt;

     public void setPassword(String password) {
         PASSWORD_DIGESTER.setPassword(password, (p, s) -> {
             this.password = p;
             this.salt = s;
         });
     }

     public boolean passwordEquals(String password) {
         return PASSWORD_DIGESTER.passwordEquals(this.password, password, salt);
     }

 }

```

## Licensing

**Password Digester** is provided and distributed under the [Apache Software License 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Refer to *LICENSE.txt* for more information.
